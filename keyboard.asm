.MODEL SMALL
.CODE
ORG 100h

TData: 
JMP Proses
       T_ASCII     DB 13,10,'Ini adalah tombol ASCII : $'
       T_Extended  DB 13,10,'Ini adalah tombol Extended $'
Proses:   
        MOV AH,0
        INT 16h
        PUSH AX

        CMP AL,00
        JE Extended

ASCII:
        LEA DX,T_ASCII
        MOV AH,09
        INT 21h

        POP AX
        MOV DL,AL
        MOV AH,2
        INT 21h

        CMP AL,'Q'
        JE exit
        CMP AL,'q'
        JE exit
        JMP Proses      
Extended :
        LEA DX,T_Extended
        MOV AH,09
        INT 21h
        JMP Proses
exit    : INT 20h
END     Tdata   
